# Viertsemesterprojekt Modellauto

## Dokumentation
Jede Seite in [Gruppe G Dokumentation](/Gruppe G Dokumentation.pdf) ist einer Aufgabe gewidmet, mit Bildern (wo sie nötig oder gefragt sind).
Alle [Datasheets](/datasheets) zu Bauteilen, die wir benutzt haben, befinden sich ebenfalls in diesem Repository.
Der Kostenplan von [Mouser](/datasheets/Kosten_Mouser.pdf), sowie von [RS Online](/datasheets/Kosten_RS.pdf) befindet sich ebenfalls unter [datasheets](/datasheets).
Zu den Aufgabenteile von Aufgabe 3 wurde keine spezielle Dokumentation angefertigt, da sie nicht gewünscht wurde.

## Aufgabe 3.1 
Siehe [Linearregler](/Linearregler).

## Aufgabe 3.2
Siehe [Geschwindigkeitssensor](/Geschwindigkeitssensor). 
Es befinden sich das Schematic, das Board und die Libraries für die Diode und den Transistor (für den Footprint) in diesem Ordner.

## Aufgabe 3.3
Siehe [Modell 2](/Modell 2). 
Das Datenblatt zum [LM358N](/datasheets/lm358-n.pdf) von Texas Instruments, der in dem Board verwendet wurde, befindet sich ebenfalls in diesem Ordner.

## Aufgabe 3.4
Siehe [Modell 1](/Modell 1).

